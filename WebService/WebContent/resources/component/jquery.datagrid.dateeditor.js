// DataGrid日期编辑插件
(function($, undefined) {
    // 继承DataGrid
    if ($.fn.DataGrid == undefined) {
        throw "必需先引入DataGrid插件!";
    }
    /**
     * DatGrid日期编辑器
     * @param {String} field       字段名
     * @param {String} value       字段值
     * @param {Object} data       编辑所在行数据
     * @param {Object} editOptions 编辑器属性
     * @param {jQuery} $parent     编辑器放置容器
     * @param {Function} save      向提交DataGrid数据的函数
     */
    $.fn.DataGrid.DateEditor = function(field, value, data, editOptions, $parent, save) {
        var view = this.view = $(this._template({
            field: field,
            value: value,
            editOptions: editOptions
        })).appendTo($parent).datepicker({
            dateFormat: editOptions.dateFormat,
            minDate: editOptions.minDate,
            maxDate: editOptions.maxDate,
            onClose: function() {
                var newValue = view.val(),
                    validate = editOptions.validate,
                    error = validate ? validate(newValue) : null;
                if (!error) {
                    view.datepicker("destroy");
                    save(newValue, {
                        date: view.datepicker("getDate")
                    });
                } else {
                    view.next(".dataerror").remove();
                    view.after("<div class='dataerror'>" + error + "</div>")
                }
            }
        }).datepicker("show");
    };
    $.fn.DataGrid.DateEditor.prototype = {
        _template: template.compile('<input type="text" placeholder="<%= field %>" value="<%= value %>"/>')
    }
})(jQuery);