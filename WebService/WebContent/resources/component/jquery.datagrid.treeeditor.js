// DataGrid日期编辑插件
(function($, undefined) {
    // 继承DataGrid
    if ($.fn.DataGrid == undefined) {
        throw "必需先引入DataGrid插件!";
    }
    /**
     * DatGrid树选择编辑器
     * @param {String} field       字段名
     * @param {String} value       字段值
     * @param {Object} data       编辑所在行数据
     * @param {Object} editOptions 编辑器属性
     * @param {jQuery} $parent     编辑器放置容器
     * @param {Function} save      向提交DataGrid数据的函数
     */
    $.fn.DataGrid.TreeEditor = function(field, value, data, editOptions, $parent, save) {
        var record,
            view = this.view = $(this._template({
                field: field,
                value: value,
                data: data,
                editOptions: editOptions
            })).appendTo($parent).focus(),
            droptree = this.droptree = view.DropTree(editOptions.options);
        view.on("change", function(event){
            event.stopPropagation();
        });
        view.on("close", function(event) {
            droptree.destroy();
            save(view.data("value"));
        });
        editOptions.datas ? droptree.dataset.setDatas(typeof editOptions.datas == "function" ? editOptions.datas() : editOptions.datas) : droptree.dataset.open();
        droptree.setValue(value);
        droptree.show();
    };
    $.fn.DataGrid.TreeEditor.prototype = {
        _template: template.compile('\<input type="text" placeholder="<%= field %>" value="<%= value %>" readonly>')
    }
})(jQuery);