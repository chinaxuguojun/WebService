// DataGrid日期编辑插件
(function($, undefined) {
    // 继承DataGrid
    if ($.fn.DataGrid == undefined) {
        throw "必需先引入DataGrid插件!";
    }
    /**
     * DatGrid树选择编辑器
     * @param {String} field       字段名
     * @param {String} value       字段值
     * @param {Object} data       编辑所在行数据
     * @param {Object} editOptions 编辑器属性
     * @param {jQuery} $parent     编辑器放置容器
     * @param {Function} save      向提交DataGrid数据的函数
     */
    $.fn.DataGrid.ComboEditor = function(field, value, data, editOptions, $parent, save) {
        var that = this,
            view = this.view = $(this._template({
                field: field,
                value: value,
                data: data,
                editOptions: editOptions
            })).appendTo($parent),
            datacombo = this.datacombo = view.DataCombo($.extend({
                value: value
            }, editOptions.options));
        view.on("change", function(event) {
            event.stopPropagation();
        });
        view.on("close", function(event) {
            var newValue = view.val(),
                validate = editOptions.validate,
                error = validate ? validate(newValue) : null;
            if (!error) {
                datacombo.destroy();
                save(newValue, that.getExInfo());
            } else {
                datacombo.view.next(".dataerror").remove();
                datacombo.view.after("<div class='dataerror'>" + error + "</div>")
            }
        });
        datacombo.on("load", function() {
            datacombo.show();
        });
        editOptions.datas ? datacombo.dataset.setDatas(typeof editOptions.datas == "function" ? editOptions.datas() : editOptions.datas) : datacombo.dataset.open();
    };
    $.fn.DataGrid.ComboEditor.prototype = {
        getExInfo: function() {
            return {
                name: this.view.find(":selected").text()
            }
        },
        _template: template.compile('\<select></select>')
    }
})(jQuery);