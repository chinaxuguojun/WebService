// 依赖ztree
(function($, undefined) {
    var ie = !!window.ActiveXObject,
        ie6 = ie && !window.XMLHttpRequest;
    $.fn.DropDown = function(options) {
        return new $.fn.DropDown.fn.init(options, this);
    };
    $.fn.DropDown.options = function(options) {
        $.extend(this, options);
    };
    $.fn.DropDown.options.prototype = {
        target: "",
        preventClose: "button,input",
        open: false,
        closeOn: "click",
        closeOnInnerAction: true
    };
    $.fn.DropDown.fn = $.fn.DropDown.prototype = {
        init: function(options, view) {
            var options = new $.fn.DropDown.options(options);
            this.options = options;
            this.view = view;
            this._initView(options);
        },
        _initView: function(options) {
            var that = this;
            this.view.on("click", function() {
                that.show();
            });
            options.open && this.show();
            $(window).on("blur", function() {
                that.hide();
            });
        },
        show: function() {
            var that = this,
                $target = $(this.options.target),
                offset = this.view.offset(),
                cssObj,
                oh = this.view.outerHeight(),
                ow = this.view.outerWidth(),
                tow,
                toh,
                top,
                left,
                ww = $(window).width(),
                closeOn = this.options.closeOn + ".dropdown",
                preventClose = this.options.preventClose,
                closeOnInnerAction = this.options.closeOnInnerAction;
            if ($target.length == 0) {
                $target = this.view.next();
            }
            $(document).off("click.dropdown");
            if ($target.hasClass("hide")) {
                toh = $target.outerHeight();
                $target.removeClass("hide").addClass("datadropdown");
                tow = $target.outerWidth();
                console.log(tow);
                top = oh + toh + offset.top > $(window).height() + $(document).scrollTop() && offset.top - toh > 0 ? offset.top - toh : offset.top + oh;
                left = tow + offset.left < ww ? offset.left : offset.left + ow - tow;
                cssObj = {
                    left: left,
                    top: top
                };
                $target.css(cssObj).outerWidth(this.view.outerWidth());
                $(".datadropdown").not($target).addClass("hide");
                setTimeout(function() {
                    $(document).on(closeOn, function(event) {
                        var $eventTarget = $(event.target);
                        if ((closeOnInnerAction && !$eventTarget.is(preventClose)) || $target.has($eventTarget).length == 0) {
                            that.hide();
                            ie6 && $("select").css("visibility", "");
                            $(document).off(closeOn);
                        }
                    });
                    ie6 && $("select").css("visibility", "hidden");
                });
                this.view.trigger("open");
            } else {
                that.hide();
            }
        },
        hide: function() {
            if (!$(this.options.target).hasClass("hide")) {
                $(".datadropdown").addClass("hide");
                this.view.trigger("close");
            }
        },
        destroy: function() {
            this.view.off("click");
            this.hide();
        }
    };
    $.fn.DropDown.fn.init.prototype = $.fn.DropDown.fn;
})(jQuery);