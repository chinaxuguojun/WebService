﻿create table TB_SM_SQLDEFINE
(
  oid               VARCHAR2(50) not null,
  pid               VARCHAR2(50),
  mid               VARCHAR2(50),
  sqlid             VARCHAR2(50),
  sqloptions        NUMBER(20),
  sqlname           VARCHAR2(1000),
  tablename         VARCHAR2(50),
  sqldesc           VARCHAR2(4000),
  updatedate        DATE,
  objectversion     NUMBER(20),
  defselectsql      CLOB,
  defselectparams   CLOB,
  defselectmetadata CLOB,
  definsertsql      CLOB,
  definsertparams   CLOB,
  defupdatesql      CLOB,
  defupdateparams   CLOB,
  defdeletesql      CLOB,
  defdeleteparams   CLOB,
  selectsql         CLOB,
  insertsql         CLOB,
  updatesql         CLOB,
  deletesql         CLOB
);

alter table TB_SM_SQLDEFINE
  add constraint PRI_SQLDEFINE primary key (OID);