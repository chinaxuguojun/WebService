<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.util.Enumeration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div>
<%
String ip = request.getHeader("cdn-src-ip");
if ((ip == null) || ip.length() == 0
		|| (ip.equalsIgnoreCase("unknown"))) {
	ip = request.getHeader("x-forwarded-for");
} else if (ip == null || ip.length() == 0
		|| "unknown".equalsIgnoreCase(ip)) {
	ip = request.getHeader("Proxy-Client-IP");
} else if (ip == null || ip.length() == 0
		|| "unknown".equalsIgnoreCase(ip)) {
	ip = request.getHeader("WL-Proxy-Client-IP");
} else if (ip == null || ip.length() == 0
		|| "unknown".equalsIgnoreCase(ip)) {
	ip = request.getRemoteAddr();
}
out.write("ip: " + ip);
%>
</div>
	<div>
		<%
			String aa = null;
			String name = null;
			Enumeration<String> estr = request.getHeaderNames();
			while (estr.hasMoreElements()) {
				name = estr.nextElement();
				aa = name + ": " + request.getHeader(name) + "\n ";
				out.write(aa);
			}
		%>
	</div>
	<%
		out.write("Proxy-Client-IP: "
				+ request.getHeader("Proxy-Client-IP") + "\n");
	%>
	<div>
		<%
			out.write("WL-Proxy-Client-IP: "
					+ request.getHeader("WL-Proxy-Client-IP") + "\n");
		%>

	</div>
	<div>
		<%
			out.write("getRemoteAddr: " + request.getRemoteAddr() + "\n");
		%>
	</div>
	<div>
		<%
			InetAddress inet = null;
			inet = InetAddress.getLocalHost();
			String ipAddress = inet.getHostAddress();
			out.write("ipAddress: " + ipAddress + "\n");
		%>
	</div>
</body>
</html>